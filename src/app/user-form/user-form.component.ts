import { Component, OnInit } from '@angular/core';
import { User } from '../user/user';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'jce-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

user:User = {name:'', email:''};

onSubmit(form:NgForm){
  console.log(form.form.value.name);
}
  constructor() { }

  ngOnInit() {
  }

}
