import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  post:Post;
  @Output()deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post[]>();
  tempPost:Post = {userId:null,title:null};
  isEdit:Boolean =false;
  editButtonText = 'Edit';
  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.post);
  }

  cancelEdit(){
    this.isEdit = false;
    this.post.userId = this.tempPost.userId;
    this.post.title = this.tempPost.title;
    this.editButtonText = 'Edit'; 
  }

  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'save' : this.editButtonText = 'Edit';
    if(this.isEdit){
       this.tempPost.userId = this.post.userId;
       this.tempPost.title = this.post.title;
     } else {
       let originalAndNew = [];
       originalAndNew.push(this.tempPost,this.post);
       this.editEvent.emit(originalAndNew);
     }

  }
  ngOnInit() {
  }

}
